package cat.dam.kevin.majormenorigual;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("ResourceType")
    GridLayout gridLayout;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Random r = new Random();
        final int lion = r.nextInt(10)+1;
        final int elephant = r.nextInt(10)+1;
        final TextView tv = findViewById(R.id.resultat);
        final ImageButton menor = (ImageButton) findViewById(R.id.menor);
        final ImageButton igual = (ImageButton) findViewById(R.id.igual);
        final ImageButton major = (ImageButton) findViewById(R.id.major);
        final GridLayout layout = (GridLayout) findViewById(R.id.GL_elephants);
        menor.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                if(lion > elephant) {
                    tv.setText("Es correcte");
                }
                else tv.setText("Es Incorrecte!!!");
            }
        });
        igual.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                if(elephant == lion) {
                    tv.setText("Es correcte");
                }
                else tv.setText("Es Incorrecte!!!");
            }
        });
        major.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                if(elephant > lion) {
                    tv.setText("Es correcte");
                }
                else tv.setText("Es Incorrecte!!!");
            }
        });


        gridLayout = (GridLayout) findViewById(R.id.GL_elephants);
        for (int i = 0; i <elephant ; i++) {
            ImageView n = new ImageView(this);
            n.setImageResource(R.drawable.elefeante);
            n.setAdjustViewBounds(true);
            int alt = 100, ample=100;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ample, alt);
            n.setLayoutParams(params);
            gridLayout.addView(n);
        }
        gridLayout = (GridLayout) findViewById(R.id.GL_lions);
        for (int i = 0; i <lion ; i++) {
            ImageView n = new ImageView(this);
            n.setImageResource(R.drawable.leon);
            n.setAdjustViewBounds(true);
            int alt = 100, ample=100;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ample, alt);
            n.setLayoutParams(params);
            gridLayout.addView(n);
        }



    }





}