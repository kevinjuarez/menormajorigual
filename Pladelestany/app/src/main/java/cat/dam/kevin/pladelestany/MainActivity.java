package cat.dam.kevin.pladelestany;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.AutoCompleteTextView;
import android.widget.ArrayAdapter;


import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    static String pobles[] = {"Banyoles", "Porqueres", "Fontcuberta", "Vilademuls",
                              "Crespia", "Cornella del Terri", "Camos", "Sant Miquel de Campmajor",
                              "Serinya", "Palol de Revardit","Esponella"};
    static  String habitans [] = {"17.451","4.208","1.212","769","247",
                                  "2.106","698","218","1.084","459","441"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, pobles);
        AutoCompleteTextView poble = (AutoCompleteTextView)
                findViewById(R.id.poble);
        poble.setAdapter(adapter);

        final AutoCompleteTextView municipis = findViewById(R.id.poble);
        final Button btn_1 = findViewById(R.id.enter);


        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nompoble = municipis.getText().toString();
                int index = Arrays.asList(pobles).indexOf(nompoble);
                TextView tv = findViewById(R.id.resultat);
                tv.setText(habitans[index]);
            }

        });

    }


}